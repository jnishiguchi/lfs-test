This is a test repository for evaluating LFS on gitlab.com

The files stored here are all open source licensed.

# Git LFS
This Git repository uses Git LFS.

The Git LFS extension will need to be installed to your Ubuntu machine:
https://git-lfs.github.com/

To install in Ubuntu:
```
$ curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
$ sudo apt-get install git-lfs
```

To enable Git LFS globally on your Ubuntu machine:
```
$ git lfs install
```

To enable git credential username/password storage:
```
$ git config --global credential.helper 'store'
```

# How to sync
After installing Git LFS, you can simply clone this repository with:
```
$ git clone https://gitlab.com/jnishiguchi/lfs-test.git
```
